package serviceauth

import (
	"context"

	"github.com/Nerzal/gocloak/v7"
	"github.com/rs/zerolog/log"
)

//IAM ..
type IAM struct {
	Client   gocloak.GoCloak
	ClientID string
	Token    *gocloak.JWT
	User     string
	Password string
	Host     string
	Realm    string
	Service  string
	Secret   string
}

//Init login to keycloak and get an offline token
func (i *IAM) Init() {
	var err error
	ctx := context.Background()
	i.Client = gocloak.NewClient(i.Host)
	i.Token, err = i.Client.GetToken(ctx, i.Realm, gocloak.TokenOptions{
		ClientID:     &i.ClientID,
		ClientSecret: &i.Secret,
		GrantType:    gocloak.StringP("password"),
		Username:     &i.User,
		Password:     &i.Password,
		Scope:        gocloak.StringP("offline_access"),
	})
	if err != nil {
		log.Panic().Err(err).Str("service", i.Service).Msg("Failed to Init")
	}
}

//RefreshToken ..
func (i *IAM) RefreshToken() (token *gocloak.JWT, err error) {
	ctx := context.Background()
	token, err = i.Client.RefreshToken(ctx, i.Token.RefreshToken, i.Realm, i.Secret, i.ClientID)
	if err != nil {
		log.Panic().Err(err).Str("service", i.Service).Msg("Refresh token Error")
	}
	return
}

func (i *IAM) GetTokenRemainingValidity() int {
	return i.Token.ExpiresIn

	// currentTime := time.Now()

	// i, err := strconv.ParseInt(i.Token.ExpiresIn, 10, 64)
	// if err != nil {
	// 	panic(err)
	// }
	// tm := time.Unix(i, 0)
	// fmt.Printf("diff utc: %v \n", tm)

	// diff := tm.Sub(currentTime)
	// fmt.Println(diff.Minutes())
	// fmt.Println(currentTime.Unix())

	// var expireOffset = 3600
	// time.Now().Add(time.Minute * 15).Unix()
	// if validity, ok := timestamp.(int64); ok {
	// 	tm := time.Unix(int64(validity), 0)
	// 	remainder := tm.Sub(time.Now())

	// 	if remainder > 0 {
	// 		return int(remainder.Seconds() + float64(expireOffset))
	// 	}
	// }
	// return expireOffset
}
