package domain

//Attributes ..
type Attributes struct {
	SpayedNeutered bool `json:"spayedNeutered,omitempty"`
	Declawed       bool `json:"declawed,omitempty"`
	SpecialNeeds   bool `json:"specialNeeds,omitempty"`
	ShotsCurrent   bool `json:"shotsCurrent,omitempty"`
}
