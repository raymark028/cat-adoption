package domain

// Microchip ..
type Microchip struct {
	ID       *string `json:"id,omitempty"`
	Registry *string `json:"registry,omitempty"`
	Brand    *string `json:"brand,omitempty"`
}
