package domain

import (
	"context"
	"regexp"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

// Cat ..
type Cat struct {
	ID          int64       `json:"id,omitempty"`
	Name        string      `json:"name,omitempty"`
	Microchip   *Microchip  `json:"microchip,omitempty"`
	Attributes  *Attributes `json:"attributes,omitempty"`
	DateCreated time.Time   `json:"dateCreated,omitempty"`
	LastUpdate  time.Time   `json:"lastUpdate,omitempty"`
	Photos      []string    `json:"photos,omitempty"`
	Age         string      `json:"age"`
	Size        string      `json:"size"`
	Breed       string      `json:"breed,omitempty"`
	Color       string      `json:"color,omitempty"`
	Coat        string      `json:"coat,omitempty"`
	Gender      string      `json:"gender"`
	Status      string      `json:"status,omitempty"`
}

//Validate validate the cat struct without ID
func (c Cat) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Status, validation.When(c.Status != "", validation.Required, validation.Match(regexp.MustCompile(`\b(adoptable|adopted)\b`)))),
	)
}

//ValidatePersisted validate the cat struct that has been persisted to database, basically ID is not empty
func (c Cat) ValidatePersisted() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Name, validation.When(c.Name != "", validation.Required)),
		validation.Field(&c.Age, validation.When(c.Age != "", validation.Required, validation.Match(regexp.MustCompile(`\b(young|adult|senior)\b`)))),
		validation.Field(&c.Size, validation.When(c.Size != "", validation.Required, validation.Match(regexp.MustCompile(`\b(small|medium|large|xlarge)\b`)))),
		validation.Field(&c.Breed, validation.When(c.Breed != "", validation.Required)),
		validation.Field(&c.Color, validation.When(c.Color != "", validation.Required)),
		validation.Field(&c.Coat, validation.When(c.Coat != "", validation.Required, validation.Match(regexp.MustCompile(`\b(hairless|sparsed|curled|short|semiLong|long)\b`)))),
		validation.Field(&c.Gender, validation.When(c.Gender != "", validation.Required, validation.Match(regexp.MustCompile(`\b(male|female)\b`)))),
		validation.Field(&c.Status, validation.When(c.Status != "", validation.Required, validation.Match(regexp.MustCompile(`\b(adoptable|adopted)\b`)))),
	)
}

// CatUsecase represent the Cat's usecases or story
type CatUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]Cat, string, error)
	GetByID(ctx context.Context, id int64) (Cat, error)
	Insert(ctx context.Context, su *Cat) error
	Update(ctx context.Context, sr *Cat) error
}

// CatRepository represent the Cats's repository contract
type CatRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []Cat, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (Cat, error)
	Insert(ctx context.Context, sr *Cat) error
	GetPhotoByCatID(ctx context.Context, id int64) (result []string, err error)
	Update(ctx context.Context, sr *Cat) error
}
