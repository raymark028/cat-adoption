FROM golang:1.15-alpine
ARG version
COPY go.* /go/src/cat_adoption/
COPY pkg/ /go/src/cat_adoption/pkg/
COPY vendor/ /go/src/cat_adoption/vendor/
COPY services/cat/ /go/src/cat_adoption/services/cat/
WORKDIR /go/src/cat_adoption/services/cat/
RUN GO111MODULE=on GOFLAGS=-mod=vendor CGO_ENABLED=0 GOOS=linux go build -v -ldflags "-X cat_adoption/services/cat/main.version=$version" -a -installsuffix cgo -o cat .

FROM alpine:3.12
RUN apk --no-cache add ca-certificates
WORKDIR /cat/
COPY --from=0 /go/src/cat_adoption/services/cat .
ENTRYPOINT ["/cat/cat"]
CMD ["run", "--logtostderr"]
 