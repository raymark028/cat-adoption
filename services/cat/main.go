package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"time"

	"github.com/Nerzal/gocloak/v7"
	"gitlab.com/raymark028/cat-adoption/pkg/gocloakecho"
	"gitlab.com/raymark028/cat-adoption/pkg/logger"
	"gitlab.com/raymark028/cat-adoption/services/cat/config"
	_catHttpDelivery "gitlab.com/raymark028/cat-adoption/services/cat/delivery/http"
	_catRepo "gitlab.com/raymark028/cat-adoption/services/cat/repository/mysql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	_catUcase "gitlab.com/raymark028/cat-adoption/services/cat/usecase"
)

func main() {

	logger := logger.New(true)

	//DB Configuration Initialization
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.DbUser, config.DbPass, config.DbHost, config.DbPort, config.DbName)
	val := url.Values{}
	val.Add("parseTime", "1") // Parse Timestamp/datetime data
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	dbConn, err := sql.Open(`mysql`, dsn)

	if err != nil {
		log.Fatal(err)
	}

	err = dbConn.Ping() // PING Connection

	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err := dbConn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	//Auth Server Middleware config
	ctx := context.Background()
	cloak := gocloak.NewClient("http://" + config.AuthHost)
	auth := gocloakecho.NewDirectGrantMiddleware(ctx, cloak, config.AuthRealm, config.AuthClient, config.AuthSecret, "email", nil)

	//HTTP Delivery Initialization
	e := echo.New()

	//initialize Repo
	cr := _catRepo.NewMysqlCatRepository(dbConn, logger)

	t, err := strconv.Atoi(config.ServiceTimeout)
	if err != nil {
		log.Fatal(err)
	}

	timeoutContext := time.Duration(t) * time.Second
	cu := _catUcase.NewCatUsecase(cr, timeoutContext)
	_catHttpDelivery.NewCatHandler(e, cu, logger, auth)

	log.Fatal(e.Start(":8080"))
}
