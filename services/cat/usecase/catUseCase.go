package usecase

import (
	"context"
	"time"

	"gitlab.com/raymark028/cat-adoption/services/cat/domain"
)

type catUsecase struct {
	catRepo        domain.CatRepository
	contextTimeout time.Duration
}

// NewCatUsecase will create new a cat object representation of domain.CatUsecase interface
func NewCatUsecase(a domain.CatRepository, timeout time.Duration) domain.CatUsecase {
	return &catUsecase{
		catRepo:        a,
		contextTimeout: timeout,
	}
}

func (cu catUsecase) Fetch(c context.Context, cursor string, num int64) (res []domain.Cat, nextCursor string, err error) {
	if num == 0 {
		num = 10
	}

	ctx, cancel := context.WithTimeout(c, cu.contextTimeout)
	defer cancel()

	res, nextCursor, err = cu.catRepo.Fetch(ctx, cursor, num)
	if err != nil {
		return
	}

	for index, catData := range res {
		photos, photoErr := cu.catRepo.GetPhotoByCatID(ctx, catData.ID)
		if photoErr == nil {
			res[index].Photos = photos
		}
	}
	return
}

func (cu catUsecase) GetByID(c context.Context, id int64) (res domain.Cat, err error) {
	ctx, cancel := context.WithTimeout(c, cu.contextTimeout)
	defer cancel()

	res, err = cu.catRepo.GetByID(ctx, id)
	if err != nil {
		return
	}
	photos, photoErr := cu.catRepo.GetPhotoByCatID(ctx, id)
	if photoErr == nil {
		res.Photos = photos
	}
	return
}

func (cu catUsecase) Update(c context.Context, sr *domain.Cat) (err error) {
	ctx, cancel := context.WithTimeout(c, cu.contextTimeout)
	defer cancel()
	sr.LastUpdate = time.Now()

	err = cu.catRepo.Update(ctx, sr)
	if err != nil {
		return
	}

	return
}

func (cu catUsecase) Insert(c context.Context, sr *domain.Cat) (err error) {
	ctx, cancel := context.WithTimeout(c, cu.contextTimeout)
	defer cancel()

	err = cu.catRepo.Insert(ctx, sr)
	if err != nil {
		return
	}

	return
}
