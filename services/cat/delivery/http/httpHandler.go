package http

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/raymark028/cat-adoption/pkg/gocloakecho"
	"gitlab.com/raymark028/cat-adoption/pkg/logger"
	"gitlab.com/raymark028/cat-adoption/services/cat/domain"
)

// ResponseError represent the reseponse error struct
type ResponseError struct {
	Message string `json:"message"`
}

// catHandler  represent the httphandler for cat
type catHandler struct {
	CUsecase domain.CatUsecase
	logger   *logger.Logger
	auth     gocloakecho.AuthenticationMiddleWare
}

// NewCatHandler will initialize the cat resources endpoint
func NewCatHandler(e *echo.Echo, cus domain.CatUsecase, log *logger.Logger, auth gocloakecho.AuthenticationMiddleWare) {
	handler := &catHandler{
		CUsecase: cus,
		logger:   log,
		auth:     auth,
	}
	e.GET("/cat", handler.FetchCat)      //, auth.CheckResource("account", "cat-management", "cat-list"))
	e.POST("/cat", handler.AddCat)       //auth.CheckResource("account", "cat-management", "cat-insert"))
	e.GET("/cat/:id", handler.GetByID)   // auth.CheckResource("account", "cat-management", "cat-list"))
	e.PUT("/cat/:id", handler.UpdateCat) // auth.CheckResource("account", "cat-management", "cat-insert"))
}

// FetchCat will fetch a cats based on given params
func (a *catHandler) FetchCat(c echo.Context) error {
	numS := c.QueryParam("num")
	num, _ := strconv.Atoi(numS)
	cursor := c.QueryParam("cursor")
	ctx := c.Request().Context()
	var nextCursor string
	var listCr []domain.Cat
	var err error

	listCr, nextCursor, err = a.CUsecase.Fetch(ctx, cursor, int64(num))
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	a.logger.Debug().Str("action", "getCat").Msgf("Number of result %v", len(listCr))
	c.Response().Header().Set(`X-Cursor`, nextCursor)
	return c.JSON(http.StatusOK, listCr)
}

// GetByID will get a cat by given id
func (a *catHandler) GetByID(c echo.Context) error {
	idP, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, domain.ErrNotFound.Error())
	}

	id := int64(idP)
	ctx := c.Request().Context()
	art, err := a.CUsecase.GetByID(ctx, id)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, art)
}

// UpdateCat will update editable cat fields
func (a *catHandler) UpdateCat(c echo.Context) error {
	// idP, err := strconv.Atoi(c.Param("id"))
	// if err != nil {
	// 	return c.JSON(http.StatusNotFound, domain.ErrNotFound.Error())
	// }

	return c.JSON(http.StatusNoContent, nil)
}

// AddCat ..
func (a *catHandler) AddCat(c echo.Context) error {
	// tokenInfo := c.Get("decodedToken").(jwt.MapClaims)
	// idP, err := strconv.Atoi(c.Param("id"))
	// if err != nil {
	// 	return c.JSON(http.StatusNotFound, domain.ErrNotFound.Error())
	// }

	// u := new(domain.Cat)
	// if err = c.Bind(u); err != nil {
	// 	return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	// }
	// if err = u.Validate(); err != nil {
	// 	return c.JSON(getStatusCode(domain.ErrBadParamInput), ResponseError{Message: err.Error()})
	// }

	// ctx := c.Request().Context()
	// a.CUsecase.AddCat(ctx, u)

	return c.JSON(http.StatusNoContent, nil)
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}
	log.Error().Err(err)
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	case domain.ErrBadParamInput:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
