FROM golang:1.15-alpine
ARG version
COPY go.* /go/src/cat_adoption/
COPY pkg/ /go/src/cat_adoption/pkg/
COPY vendor/ /go/src/cat_adoption/vendor/
COPY services/cat/ /go/src/cat_adoption/services/cat/
WORKDIR /go/src/cat_adoption/services/cat/
RUN go get github.com/githubnemo/CompileDaemon

ENTRYPOINT CompileDaemon --build="go build main.go" --command=./main