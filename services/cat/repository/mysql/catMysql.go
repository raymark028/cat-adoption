package mysql

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/raymark028/cat-adoption/pkg/logger"
	"gitlab.com/raymark028/cat-adoption/services/cat/domain"
	"gitlab.com/raymark028/cat-adoption/services/cat/repository"
)

type mysqlCatRepository struct {
	Conn   *sql.DB
	Logger *logger.Logger
}

const (
	QueryCatByID = `SELECT a.id,a.name,a.status,a.age,a.size,a.breed,a.color,a.coat,a.gender,a.date_created,a.date_updated,b.spayed_neutered,b.declawed,b.special_needs,b.shots_current,c.chip_id,c.registry,c.brand
	FROM cats a 
	LEFT JOIN attributes b on a.id = b.cat_id
	LEFT JOIN microchips c on a.id = c.cat_id 
	WHERE a.id = ?`

	QueryCat = `SELECT a.id,a.name,a.status,a.age,a.size,a.breed,a.color,a.coat,a.gender,a.date_created,a.date_updated,b.spayed_neutered,b.declawed,b.special_needs,b.shots_current,c.chip_id,c.registry,c.brand
	FROM cats a 
	LEFT JOIN attributes b on a.id = b.cat_id
	LEFT JOIN microchips c on a.id = c.cat_id 
	WHERE a.date_created > ? ORDER BY a.date_created LIMIT ?`

	QueryCatPhotoByID = "SELECT url FROM photos where cat_id = ?"

	UpdateCat = ""
	InsertCat = ""
)

// NewMysqlCatRepository will create an object that represent the cat.Repository interface
func NewMysqlCatRepository(conn *sql.DB, log *logger.Logger) domain.CatRepository {
	return &mysqlCatRepository{
		Conn:   conn,
		Logger: log,
	}
}

func (c *mysqlCatRepository) fetch(ctx context.Context, query string, args ...interface{}) (result []domain.Cat, err error) {
	rows, err := c.Conn.QueryContext(ctx, query, args...)
	if err != nil {
		c.Logger.Info().Err(err).Str("action", "buildFetchQuery").Msg("An Error has occured")
		return nil, err
	}

	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			c.Logger.Info().Err(errRow).Str("action", "closingFetchQuery").Msg("An Error has occured")
		}
	}()

	result = make([]domain.Cat, 0)
	for rows.Next() {
		a := domain.Cat{}
		b := domain.Attributes{}
		d := domain.Microchip{}

		err = rows.Scan(
			&a.ID,
			&a.Name,
			&a.Status,
			&a.Age,
			&a.Size,
			&a.Breed,
			&a.Color,
			&a.Coat,
			&a.Gender,
			&a.DateCreated,
			&a.LastUpdate,
			&b.SpayedNeutered,
			&b.Declawed,
			&b.SpecialNeeds,
			&b.ShotsCurrent,
			&d.ID,
			&d.Registry,
			&d.Brand,
		)
		a.Attributes = &b
		a.Microchip = &d
		if err != nil {
			c.Logger.Info().Err(err).Str("action", "fetchRowsQuery").Msg("An Error has occured")
			return nil, err
		}
		result = append(result, a)
	}

	return result, nil
}

func (c *mysqlCatRepository) Fetch(ctx context.Context, cursor string, num int64) (res []domain.Cat, nextCursor string, err error) {

	decodedCursor, err := repository.DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", domain.ErrBadParamInput
	}

	res, err = c.fetch(ctx, QueryCat, decodedCursor, num)
	if err != nil {
		return nil, "", err
	}

	if len(res) == int(num) {
		nextCursor = repository.EncodeCursor(res[len(res)-1].LastUpdate)
	}

	return
}

func (c *mysqlCatRepository) GetByID(ctx context.Context, id int64) (res domain.Cat, err error) {

	list, err := c.fetch(ctx, QueryCatByID, id)
	if err != nil {
		return domain.Cat{}, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}

	return
}

func (c *mysqlCatRepository) GetPhotoByCatID(ctx context.Context, id int64) (result []string, err error) {

	rows, err := c.Conn.QueryContext(ctx, QueryCatPhotoByID, id)
	if err != nil {
		c.Logger.Info().Err(err).Str("action", "buildFetchQuery").Msg("An Error has occured")
		return
	}

	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			c.Logger.Info().Err(errRow).Str("action", "closingFetchQuery").Msg("An Error has occured")
			return
		}
	}()

	result = make([]string, 0)
	for rows.Next() {
		var a string
		err = rows.Scan(
			&a,
		)
		if err != nil {
			c.Logger.Info().Err(err).Str("action", "fetchRowsQuery").Msg("An Error has occured")
			return
		}
		result = append(result, a)
	}

	return
}

func (c *mysqlCatRepository) Update(ctx context.Context, cr *domain.Cat) (err error) {
	stmt, err := c.Conn.PrepareContext(ctx, UpdateCat)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, cr.Status, cr.ID)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	c.Logger.Debug().Err(err).Int64("affectedData", affect).Msgf("Cat updated data: %v", cr)
	if err != nil {
		return
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behavior. Total Affected: %d", affect)
		return
	}

	return
}

func (c *mysqlCatRepository) Insert(ctx context.Context, cr *domain.Cat) error {

	stmt, err := c.Conn.PrepareContext(ctx, InsertCat)
	if err != nil {
		return err
	}

	res, err := stmt.ExecContext(ctx, cr.LastUpdate)
	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	cr.ID = int64(id)
	c.Logger.Debug().Err(err).Int64("CatID", id).Msgf("Cat inserted: %v", cr)
	return nil
}
