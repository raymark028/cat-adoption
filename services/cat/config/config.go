package config

import (
	"os"
)

var Service = "cat"
var ServiceTimeout = os.Getenv("TIMEOUT")

//Auth Server
var AuthHost = os.Getenv("AUTH_HOST")
var AuthRealm = os.Getenv("AUTH_REALM")
var AuthSecret = os.Getenv("AUTH_SECRET")
var AuthClient = os.Getenv("AUTH_CLIENT")

//Services Auth
var SAuthHost = os.Getenv("SAUTH_HOST")
var SAuthRealm = os.Getenv("SAUTH_REALM")
var SAuthUser = os.Getenv("SAUTH_USER")
var SAuthPass = os.Getenv("SAUTH_PASS")
var SAuthClient = os.Getenv("SAUTH_CLIENT")

//DB conf
var DbHost = os.Getenv("DB_HOST")
var DbPort = "3306"
var DbUser = os.Getenv("DB_USER")
var DbPass = os.Getenv("DB_PASS")
var DbName = os.Getenv("DB")
var SocketHost = os.Getenv("SOCKET_HOST")
