module gitlab.com/raymark028/cat-adoption

go 1.15

require (
	github.com/Nerzal/gocloak/v7 v7.11.0
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/fatih/color v1.10.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/githubnemo/CompileDaemon v1.2.1 // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/rs/zerolog v1.20.0
)
