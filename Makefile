# This included makefile should define the 'custom' target rule which is called here.
# The (-) sign before `include` will do an implicit check if the file exists.
-include $(INCLUDE_MAKEFILE)

docker:
	docker-compose up --build -d

run:
	docker-compose up -d

stop:
	docker-compose down


# BUILD SERVICES FOR REGISTRY
build-cat: 
	docker build -t registry.gitlab.com/raymark028/cat-adoption:cat0.1 -f services/cat/cat.dockerfile .
push-cat: 
	docker push registry.gitlab.com/raymark028/cat-adoption:cat0.1