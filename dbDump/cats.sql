-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cats`;
CREATE TABLE `cats` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` enum('adoptable','adopted') COLLATE utf8_bin NOT NULL,
  `age` enum('young','adult','senior') COLLATE utf8_bin NOT NULL,
  `size` enum('small','medium','large','xlarge') COLLATE utf8_bin NOT NULL,
  `breed` varchar(255) COLLATE utf8_bin NOT NULL,
  `color` varchar(255) COLLATE utf8_bin NOT NULL,
  `coat` enum('hairless','sparsed','curled','short','semiLong','long') COLLATE utf8_bin NOT NULL,
  `gender` enum('male','female') COLLATE utf8_bin NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `age` (`age`),
  KEY `size` (`size`),
  KEY `coat` (`coat`),
  KEY `gender` (`gender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cats` (`id`, `name`, `status`, `age`, `size`, `breed`, `color`, `coat`, `gender`, `date_created`, `date_updated`) VALUES
(1,	'snowbell little',	'adoptable',	'adult',	'medium',	'persian',	'white',	'long',	'male',	'2020-12-27 20:33:43',	'2020-12-27 20:33:43');

-- 2020-12-27 20:42:51
