-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `attributes`;
CREATE TABLE `attributes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int unsigned NOT NULL,
  `spayed_neutered` tinyint(1) NOT NULL,
  `declawed` tinyint(1) NOT NULL,
  `special_needs` tinyint(1) NOT NULL,
  `shots_current` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `attributes` (`id`, `cat_id`, `spayed_neutered`, `declawed`, `special_needs`, `shots_current`) VALUES
(1,	1,	0,	0,	0,	1);

-- 2020-12-27 20:42:55
