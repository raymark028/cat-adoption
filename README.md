# cat-adoption
## Description
This is an example of implementation of Clean Architecture in Go (Golang) projects.

Rule of Clean Architecture by Uncle Bob
 * Independent of Frameworks. The architecture does not depend on the existence of some library of feature laden software. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
 * Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.
 * Independent of UI. The UI can change easily, without changing the rest of the system. A Web UI could be replaced with a console UI, for example, without changing the business rules.
 * Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. Your business rules are not bound to the database.
 * Independent of any external agency. In fact your business rules simply don’t know anything at all about the outside world.

More at https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html

This project has  4 Domain layer :
 * Domain Layer
 * Repository Layer
 * Usecase Layer  
 * Delivery Layer

#### The diagram:

![golang clean architecture](https://github.com/bxcodec/go-clean-arch/raw/master/clean-arch.png)

The original explanation about this project's structure  can read from this medium's post : https://medium.com/@imantumorang/golang-clean-archithecture-efd6d7c43047.

It may different already, but the concept still the same in application level, also you can see the change log from v1 to current version in Master.

### How To Run This Project
> Make Sure you have run dbDump/*.sql in your mysql


Since the project already use Go Module, I recommend to put the source code in any folder but GOPATH.
#### Run the Applications
Here is the steps to run it with `docker-compose`

```bash
#move to directory
$ cd workspace

# Clone into YOUR $GOPATH/src
$ git clone git@gitlab.com:raymark028/cat-adoption.git  

# Build and run the application
$ make docker

# check if the containers are running
$ docker ps

# Stop
$ make stop

# Run the application without rebuilding
$ make run
```


#### local data store and other folder
 * `appDB` - contains the mysql database
 * `keycloakDB` - contains the keycloak auth service database
 * `openAPI` - contains the open api documentation
 * `pkg` - contains packages not on vendor that is used by multiple services
 * `cmd` - contains cli tools/scripts.
 